import java.text.DecimalFormat;
import java.time.LocalTime;

public class Java8 {
    public static void main(String[] args) {
        String time="1:45";
        String[] timeParts = time.split(":");
        int hours = Integer.parseInt(timeParts[0]);
        int minutes = Integer.parseInt(timeParts[1]);

        double decimalHours = hours + (minutes / 60.0);

        System.out.println( Math.round(decimalHours * 100.0) / 100.0);

        int time1 = 1;
        double hours1 = time1 / 60.0;
        System.out.println(Math.round(hours1 * 100.0) / 100.0);

        double time2 =1.75;
        int h = (int) time2;
        double mm = ((time2-h)*60);
        int m= (int) Math.round(mm);


        System.out.println(h+":"+String.format("%02d", m));
    }
}
