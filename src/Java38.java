public class Java38 {
    public static void main(String[] args) {
        int n=46288;
        int p=3;
        String numberString=String.valueOf(n);
        int sum = 0;
        for(int i=0; i<numberString.length(); i++) {
            int digit = Character.getNumericValue(numberString.charAt(i));
            sum += Math.pow(digit, p + i);
        }
           if(sum%n==0){
            System.out.println(sum/n);
        } else {
            System.out.println("-1");
        }
    }
}
