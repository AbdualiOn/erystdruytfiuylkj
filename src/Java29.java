import java.math.BigInteger;

import static java.math.BigInteger.valueOf;

public class Java29 {
    public static void main(String[] args) {
        BigInteger n = valueOf(1324365746);
        BigInteger multiplier = BigInteger.valueOf(3);
        BigInteger m = BigInteger.valueOf(2);
        if (n.longValue() <=0 || n.longValue() ==1 ||n.longValue() ==2 ) {
            System.out.println(0);
        } else {
            BigInteger ser= n.multiply(n).subtract((n.multiply(multiplier))).add(m);
            System.out.println(ser);
        }

    }
}