public class Java28 {
    public static void main(String[] args) {
    A ab = getDep();
    B b = new B(ab);
    }

    static A getDep(){
        A a = new A();
        a.setA(2);
        return a;
    }
}

class A{
    int a;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }
}

class B{
    A a = ;

    public B(A a) {
        this.a = a;
        System.out.println(a.getA());
    }
}
